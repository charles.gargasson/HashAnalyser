# HashAnalyser / School Project

Fonctionnement
 - Scan des fichiers d'un répertoire vers une liste FILES
 - Ajouter les hashs dans la liste FILES
 - Pour chaque entrée de la WHITELIST, vérifier si elle est présente dans FILES et l'ajouter à la liste LEGIT
 - Retirer les éléments de FILES présents dans LEGIT
 - Pour chaque entrée de la BLACKLIST, vérifier si elle est présente dans FILES et l'ajouter à la liste MALICIOUS
 - Retirer les éléments de FILES présents dans MALICIOUS
 - FILES correspond maintenant aux UNKNOW
 - Générer les 3 CSV
